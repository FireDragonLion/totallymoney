﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System;
using CrazyCardsCalculator.Dto;
using System.IO;
using Moq;
using CrazyCardsCalculator.Repository;
using CrazyCardsCalculator.Configurations;
using CrazyCardsCalculator;

namespace CrazyCardsCalculator_Tests
{
    [TestFixture]
    public class CardsCalculatorTest
    {
        private IEnumerable<Customer> _customerList;
        private IEnumerable<Card> _cardList;
        private Mock<ICardsRepository> _cardsRepository;
        private Mock<ICustomerRepository> _customerRepository;
        private CardsCalculator _cardsCalculator;

        [SetUp]
        public void Init()
        {
            _customerList = new List<Customer>
            {
                new Customer{
                    Title = Title.Mr,
                    FirstName = "Ollie",
                    LastName = "Murphree",
                    DateOfBirth = "01/07/1970",
                    AnnualIncome = 34000,
                    EmploymentStatus  = EmploymentStatus.FullTime,
                    HouseNumber= "700",
                    PostCode = "BS14 9PR"
                },
                new Customer{
                    Title = Title.Miss,
                    FirstName = "Elizabeth",
                    LastName = "Edmundson",
                    DateOfBirth = "29/06/1984",
                    AnnualIncome = 17000,
                    EmploymentStatus  = EmploymentStatus.Student,
                    HouseNumber= "177",
                    PostCode = "PH12 8UW"},

                new Customer{
                    Title = Title.Mr,
                    FirstName = "Trevor",
                    LastName = "Rieck",
                    DateOfBirth = "07/09/1990",
                    AnnualIncome = 15000,
                    EmploymentStatus  = EmploymentStatus.PartTime,
                    HouseNumber= "343",
                    PostCode = "TS25 2NF"}
            };

            _cardList = new List<Card>
            {
                new Card
                {
                    Apr = 18.9m,
                    BalanceTransferOfferDuration = 0,
                    PurchaseOfferDuration=0,
                    CreditAvailable = 1200,
                    CardType = CardType.StudentLife
                },
                new Card
                {
                     Apr = 33.9m,
                    BalanceTransferOfferDuration = 12,
                    PurchaseOfferDuration=6,
                    CreditAvailable = 3000,
                    CardType = CardType.Liquid
                },
                new Card
                {
                    Apr = 33.9m,
                    BalanceTransferOfferDuration = 0,
                    PurchaseOfferDuration=0,
                    CreditAvailable = 300,
                    CardType = CardType.Anywhere
                }
            };
            _cardsRepository = new Mock<ICardsRepository>();
            _customerRepository = new Mock<ICustomerRepository>();
            _cardsRepository.Setup(x => x.GetCardList("")).Returns(_cardList);
            _customerRepository.Setup(x => x.GetCustomers("")).Returns(_customerList);
            _cardsCalculator = new CardsCalculator(_customerRepository.Object, _cardsRepository.Object, "", "");
        }

        [Test]
        [Category("Unit")]
        public void GetCustomerCards_when_one_or_more_parameters_do_not_match_customer_should_throw_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _cardsCalculator.GetCustomerCards("so", "so", "01/10/2000", 0, "0", "ki34"), "We could not find a customer the data you have inputted");
        }

        [Test]
        [Category("Unit")]
        public void When_customer_exists_and_has_Student_status_should_return_Student_Life_and_Anywhere_Cards()
        {
            //arrange
            var cards = new List<Card>
                {
                    new Card
                    {
                        Apr = 18.9m,
                        BalanceTransferOfferDuration = 0,
                        PurchaseOfferDuration=0,
                        CreditAvailable = 1200,
                        CardType = CardType.StudentLife
                    },
                    new Card
                    {
                         Apr = 33.9m,
                        BalanceTransferOfferDuration = 12,
                        PurchaseOfferDuration=6,
                        CreditAvailable = 3000,
                        CardType = CardType.Liquid
                    },
                    new Card
                    {
                        Apr = 33.9m,
                        BalanceTransferOfferDuration = 0,
                        PurchaseOfferDuration=0,
                        CreditAvailable = 300,
                        CardType = CardType.Anywhere
                    }
                };
            var expected = new CustomerCards
            {
                Customer = new Customer
                {
                    Title = Title.Miss,
                    FirstName = "Elizabeth",
                    LastName = "Edmundson",
                    DateOfBirth = "29/06/1984",
                    AnnualIncome = 17000,
                    EmploymentStatus = EmploymentStatus.Student,
                    HouseNumber = "177",
                    PostCode = "PH12 8UW"
                },
                Cards = cards
            };

            _cardsRepository.Setup(x => x.GetCardsByProfile(EmploymentStatus.Student, 17000, "")).Returns(cards);

            var result = _cardsCalculator.GetCustomerCards("Elizabeth", "Edmundson", "29/06/1984", 1, "177", "PH12 8UW");

            Assert.AreEqual(2, result.Cards.Where(x => x.CardType.Equals(CardType.Anywhere) || x.CardType.Equals(CardType.StudentLife)).Count());
        }

        [Test]
        [Category("Unit")]
        public void When_customer_exists_and_has_Partime_status_should_return_Anywhere_Cards()
        {
            //arrange
            var cards = new List<Card>
                {
                    new Card
                    {
                         Apr = 33.9m,
                        BalanceTransferOfferDuration = 0,
                        PurchaseOfferDuration=0,
                        CreditAvailable = 300,
                        CardType = CardType.Anywhere
                    },
                };
            var expected = new CustomerCards
            {
                Customer = new Customer
                {
                    Title = Title.Mr,
                    FirstName = "Trevor",
                    LastName = "Rieck",
                    DateOfBirth = "07/09/1990",
                    AnnualIncome = 15000,
                    EmploymentStatus = EmploymentStatus.PartTime,
                    HouseNumber = "343",
                    PostCode = "TS25 2NF"
                },
                Cards = cards
            };

            _cardsRepository.Setup(x => x.GetCardsByProfile(EmploymentStatus.PartTime, 15000, "")).Returns(cards);

            var result = _cardsCalculator.GetCustomerCards("Trevor", "Rieck", "07/09/1990", 2, "343", "TS25 2NF");

            Assert.AreEqual(1, result.Cards.Where(x => x.CardType.Equals(CardType.Anywhere)).Count());
        }

        [Test]
        [Category("Unit")]
        public void When_customer_exists_and_has_income_greater_than_16_should_return_Liquid_and_Anywhere_Cards()
        {
            //arrange
            var cards = new List<Card>
                {
                    new Card
                    {
                        Apr = 18.9m,
                        BalanceTransferOfferDuration = 0,
                        PurchaseOfferDuration=0,
                        CreditAvailable = 1200,
                        CardType = CardType.StudentLife
                    },
                    new Card
                    {
                         Apr = 33.9m,
                        BalanceTransferOfferDuration = 12,
                        PurchaseOfferDuration=6,
                        CreditAvailable = 3000,
                        CardType = CardType.Liquid
                    },
                    new Card
                    {
                        Apr = 33.9m,
                        BalanceTransferOfferDuration = 0,
                        PurchaseOfferDuration=0,
                        CreditAvailable = 300,
                        CardType = CardType.Anywhere
                    }
                };
            var expected = new CustomerCards
            {
                Customer = new Customer
                {
                    Title = Title.Mr,
                    FirstName = "Ollie",
                    LastName = "Murphree",
                    DateOfBirth = "01/07/1970",
                    AnnualIncome = 34000,
                    EmploymentStatus = EmploymentStatus.FullTime,
                    HouseNumber = "700",
                    PostCode = "BS14 9PR"
                },
                Cards = cards
            };

            _cardsRepository.Setup(x => x.GetCardsByProfile(EmploymentStatus.FullTime, 34000, "")).Returns(cards);

            var result = _cardsCalculator.GetCustomerCards("Ollie", "Murphree", "01/07/1970", 0, "700", "BS14 9PR");

            Assert.AreEqual(2, result.Cards.Where(x => x.CardType.Equals(CardType.Anywhere) || x.CardType.Equals(CardType.Liquid)).Count());
        }

        [Test]
        [Category("Unit")]
        public void When_customer_exists_and_has_income_greater_than_16_and_Student_status_should_return_Liquid_and_Anywhere_Cards_and_Student()
        {
            //arrange
            var cards = new List<Card>
                {
                    new Card
                    {
                        Apr = 18.9m,
                        BalanceTransferOfferDuration = 0,
                        PurchaseOfferDuration=0,
                        CreditAvailable = 1200,
                        CardType = CardType.StudentLife
                    },
                    new Card
                    {
                         Apr = 33.9m,
                        BalanceTransferOfferDuration = 12,
                        PurchaseOfferDuration=6,
                        CreditAvailable = 3000,
                        CardType = CardType.Liquid
                    },
                    new Card
                    {
                        Apr = 33.9m,
                        BalanceTransferOfferDuration = 0,
                        PurchaseOfferDuration=0,
                        CreditAvailable = 300,
                        CardType = CardType.Anywhere
                    }
                };
            var expected = new CustomerCards
            {
                Customer = new Customer
                {
                    Title = Title.Miss,
                    FirstName = "Elizabeth",
                    LastName = "Edmundson",
                    DateOfBirth = "29/06/1984",
                    AnnualIncome = 17000,
                    EmploymentStatus = EmploymentStatus.Student,
                    HouseNumber = "177",
                    PostCode = "PH12 8UW"
                },
                Cards = cards
            };

            _cardsRepository.Setup(x => x.GetCardsByProfile(EmploymentStatus.Student, 17000, "")).Returns(cards);

            var result = _cardsCalculator.GetCustomerCards("Elizabeth", "Edmundson", "29/06/1984", 1, "177", "PH12 8UW");

            Assert.AreEqual(3, result.Cards.Where(x => x.CardType.Equals(CardType.Anywhere) || x.CardType.Equals(CardType.StudentLife) || x.CardType.Equals(CardType.Liquid)).Count());
        }
    }
}
