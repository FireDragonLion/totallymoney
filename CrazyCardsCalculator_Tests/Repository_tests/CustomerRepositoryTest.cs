﻿using CrazyCardsCalculator.Configurations;
using CrazyCardsCalculator.Dto;
using CrazyCardsCalculator.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace CrazyCardsCalculator_Tests.Repository_tests
{
    [TestFixture]
    public class CustomerRepositoryTest
    {
        private Mock<IConfiguration> _configuration;
        private CustomerRepository _customerRepository;
        private const string filePathKey = "DataFilePath";

        [SetUp]
        public void Init()
        {
            _configuration = new Mock<IConfiguration>();
            _customerRepository = new CustomerRepository(_configuration.Object);
        }

        [Test]
        [Category("Unit")]
        public void GetCustomerList_should_throw_exception_if_deserializing_json_is_null()
        {
            var path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), @"..\\..\\FakeRepository\\EmptyFake.json"));
            _configuration.Setup(x => x.GetFilePath(filePathKey)).Returns(path);
            Assert.Throws<ArgumentNullException>(() => _customerRepository.GetCustomers(filePathKey), "There was a problem deserializing json");

        }

        [Test]
        [Category("Unit")]
        public void GetCustomerList_should_return_IEnumerable_of_Customer_if_deserializing_json_has_content()
        {
            var path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), @"..\\..\\FakeRepository\\CustomerListFake.json"));
            _configuration.Setup(x => x.GetFilePath(filePathKey)).Returns(path);

            var result = _customerRepository.GetCustomers(filePathKey);

            Assert.IsInstanceOf(typeof(IEnumerable<Customer>), result);
        }
    }
}
