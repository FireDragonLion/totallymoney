﻿using CrazyCardsCalculator.Configurations;
using CrazyCardsCalculator.Dto;
using CrazyCardsCalculator.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CrazyCardsCalculator_Tests.Repository_tests
{
    [TestFixture]
    public class CardsRepositoryTest
    {
        private Mock<IConfiguration> _configuration;
        private CardsRepository _cardsRepository;
        private const string filePathKey = "DataFilePath";

        [SetUp]
        public void Init()
        {
            _configuration = new Mock<IConfiguration>();
            _cardsRepository = new CardsRepository(_configuration.Object);
        }

        [Test]
        [Category("Unit")]
        public void GetCardList_should_throw_exception_if_deserializing_json_is_null()
        {
            var path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), @"..\\..\\FakeRepository\\EmptyFake.json"));
            _configuration.Setup(x => x.GetFilePath(filePathKey)).Returns(path);
            Assert.Throws<ArgumentNullException>(() => _cardsRepository.GetCardList(filePathKey), "There was a problem deserializing json");

        }

        [Test]
        [Category("Unit")]
        public void GetCardList_should_return_IEnumerable_of_Card_if_deserializing_json_has_content()
        {
            var path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), @"..\\..\\FakeRepository\\CardListFake.json"));
            _configuration.Setup(x => x.GetFilePath(filePathKey)).Returns(path);

            var result = _cardsRepository.GetCardList(filePathKey);

            Assert.IsInstanceOf(typeof(IEnumerable<Card>), result);
        }

        [Test]
        [TestCase(EmploymentStatus.Student, 14000, 2 , Category="Unit")]
        [TestCase(EmploymentStatus.FullTime, 90000, 2, Category = "Unit")]
        [TestCase(EmploymentStatus.Student, 16500, 3, Category = "Unit")]
        [TestCase(EmploymentStatus.PartTime, 18000, 2, Category = "Unit")]
        [TestCase(EmploymentStatus.PartTime, 5000, 1, Category = "Unit")]
        [TestCase(EmploymentStatus.FullTime, 14000, 1, Category = "Unit")]
        public void GetCardsByProfile_should_return_IEnumerable_Card_for_matching_profiles(EmploymentStatus status, decimal annualIncome, int expectedCardListCount)
        {
            var path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), @"..\\..\\FakeRepository\\CardListFake.json"));
            _configuration.Setup(x => x.GetFilePath(filePathKey)).Returns(path);

            var result = _cardsRepository.GetCardsByProfile(status, annualIncome, filePathKey);

            Assert.AreEqual(expectedCardListCount, result.Count());
        }

    }
}
