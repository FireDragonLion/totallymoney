﻿using CrazyCardsCalculator.Dto;

namespace CrazyCardsCalculator
{
    public interface ICardsCalculator
    {
        CustomerCards GetCustomerCards(string firstName, string lastName, string dateOfBirth, int employmentStatus, string houseNumber, string postCode);
    }
}
