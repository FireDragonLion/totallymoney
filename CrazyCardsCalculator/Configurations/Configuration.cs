﻿using System.Configuration;
using System.IO;

namespace CrazyCardsCalculator.Configurations
{
    public class Configuration : IConfiguration
    {
        public string GetFilePath(string key)
        {
            var fullPath = Path.GetFullPath(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory.ToString(), ConfigurationManager.AppSettings[key].ToString()));
            return fullPath;
        }
    }
}
