﻿namespace CrazyCardsCalculator.Configurations
{
    public interface IConfiguration
    {
        string GetFilePath(string key);
    }
}
