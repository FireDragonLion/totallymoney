﻿namespace CrazyCardsCalculator.Dto
{
    public enum EmploymentStatus
    {
        FullTime,
        Student,
        PartTime
    }

}
