﻿using System.Collections.Generic;

namespace CrazyCardsCalculator.Dto
{
    public class CustomerCards
    {
        public IEnumerable<Card> Cards { get; set; }
        public Customer Customer { get; set; }
    }
}
