﻿namespace CrazyCardsCalculator.Dto
{
    public enum CardType
    {
        Anywhere,
        Liquid,
        StudentLife
    }
}
