﻿namespace CrazyCardsCalculator.Dto
{
    public class Customer
    {
        public Title Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public decimal AnnualIncome { get; set; }
        public EmploymentStatus EmploymentStatus { get; set; }
        public string HouseNumber { get; set; }
        public string PostCode { get; set; }
    }
}
