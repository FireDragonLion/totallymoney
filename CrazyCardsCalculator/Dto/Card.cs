﻿namespace CrazyCardsCalculator.Dto
{
    public class Card
    {
        public decimal Apr { get; set; }
        public int BalanceTransferOfferDuration { get; set; }
        public int PurchaseOfferDuration { get; set; }
        public decimal CreditAvailable { get; set; }
        public CardType CardType { get; set; }
    }
}
