﻿using CrazyCardsCalculator.Configurations;
using CrazyCardsCalculator.Dto;
using CrazyCardsCalculator.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace CrazyCardsCalculator
{
    public class CardsCalculator:ICardsCalculator
    {
        private IEnumerable<Customer> _customerList;
        private ICardsRepository _cardsRepository;
        private readonly string _cardKey;
        public CardsCalculator(ICustomerRepository customerRepository, ICardsRepository cardsRepository, string customerKey, string cardKey)
        {
            _customerList = customerRepository.GetCustomers(customerKey);
            _cardsRepository = cardsRepository;
            _cardKey = cardKey;
        }
        public CustomerCards GetCustomerCards(string firstName, string lastName, string dateOfBirth, int employmentStatus, string houseNumber, string postCode)
        {
            var customer = _customerList.FirstOrDefault(x => x.FirstName == firstName
                                                        && x.LastName == lastName
                                                        && x.DateOfBirth == dateOfBirth
                                                        && (int)x.EmploymentStatus == employmentStatus
                                                        && x.HouseNumber == houseNumber
                                                        && x.PostCode == postCode);
            if (customer == null)
            {
                throw new ArgumentException("We could not find a customer the data you have inputted");
            }

            var customerCards = new CustomerCards
            {
                Customer = customer,
                Cards = _cardsRepository.GetCardsByProfile(customer.EmploymentStatus, customer.AnnualIncome, _cardKey)
            };

            return customerCards;
        }
    }
}
