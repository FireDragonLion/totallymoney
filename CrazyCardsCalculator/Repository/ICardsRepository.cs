﻿using CrazyCardsCalculator.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrazyCardsCalculator.Repository
{
   public  interface ICardsRepository
    {
        IEnumerable<Card> GetCardsByProfile(EmploymentStatus employmentStatus, decimal annualIncome, string key);
        IEnumerable<Card> GetCardList(string filePathKey);
    }
}
