﻿using CrazyCardsCalculator.Configurations;
using CrazyCardsCalculator.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CrazyCardsCalculator.Repository
{
   public class CardsRepository:ICardsRepository
    {
        private IEnumerable<Card> _cardList;
        private readonly IConfiguration _configuration;
        public CardsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IEnumerable<Card> GetCardsByProfile(EmploymentStatus employmentStatus, decimal annualIncome, string key)
        {
            _cardList = GetCardList(key);

            var cards = new List<Card>();

            cards.Add(_cardList.First(x => x.CardType == CardType.Anywhere));

            if (employmentStatus.Equals(EmploymentStatus.Student))
            {
                cards.Add(_cardList.First(x => x.CardType == CardType.StudentLife));
            }

            if (annualIncome > 16000)
            {
                cards.Add(_cardList.First(x => x.CardType == CardType.Liquid));
            }

            return cards;
        }

        public IEnumerable<Card> GetCardList(string filePathKey)
        {
            var fileData = File.ReadAllText(_configuration.GetFilePath(filePathKey));
            var cardList = JsonConvert.DeserializeObject<IEnumerable<Card>>(fileData);
            if (cardList != null)
            {
                return cardList;
            }
            else
            {
                throw new ArgumentNullException("There was a problem deserializing json");
            }
        }
       
    }
}
