﻿using CrazyCardsCalculator.Dto;
using System.Collections.Generic;

namespace CrazyCardsCalculator.Repository
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetCustomers(string key);
    }
}
