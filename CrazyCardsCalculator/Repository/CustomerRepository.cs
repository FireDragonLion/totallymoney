﻿using CrazyCardsCalculator.Configurations;
using CrazyCardsCalculator.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace CrazyCardsCalculator.Repository
{
    public class CustomerRepository:ICustomerRepository
    {
        private readonly IConfiguration _configuration;
        public CustomerRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IEnumerable<Customer> GetCustomers(string key)
        {
            var fileData = File.ReadAllText(_configuration.GetFilePath(key));
            var customerList = JsonConvert.DeserializeObject<IEnumerable<Customer>>(fileData);
            if (customerList != null)
            {
                return customerList;
            }
            else
            {
                throw new ArgumentNullException("There was a problem deserializing json");
            }
        }
    }
}
