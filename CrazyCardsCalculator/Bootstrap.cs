﻿using CrazyCardsCalculator.Configurations;
using CrazyCardsCalculator.Repository;
using Unity;
using Unity.Injection;

namespace CrazyCardsCalculator
{
    public class Bootstrap
    {
        public static IUnityContainer container;
      
        public static IUnityContainer Initialise()
        {
            container = new UnityContainer();
            // Register

            container.RegisterType<IConfiguration, Configuration>();
            container.RegisterType<ICustomerRepository, CustomerRepository>(new InjectionConstructor(
                                                              new ResolvedParameter<IConfiguration>()));
            container.RegisterType<ICardsRepository, CardsRepository>(new InjectionConstructor(
                                                              new ResolvedParameter<IConfiguration>()));
            container.RegisterType<ICardsCalculator, CardsCalculator>(new InjectionConstructor(
                                                              new ResolvedParameter<ICustomerRepository>(), new ResolvedParameter<ICardsRepository>(),  "CustomerFilePath", "CardsFilePath"));
            return container;
        }


        public static void CleanContainer()
        {
            container.Dispose();
        }
    }
}
