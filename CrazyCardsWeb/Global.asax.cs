﻿using CrazyCardsCalculator;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CrazyCardsWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            var container = Bootstrap.Initialise();
            DependencyResolver.SetResolver(new Unity.AspNet.Mvc.UnityDependencyResolver(container));
            IDependencyResolver resolver = DependencyResolver.Current;
            DependencyResolver.SetResolver(resolver);
        }
    }
}
