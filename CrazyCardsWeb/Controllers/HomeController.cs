﻿using CrazyCardsCalculator;
using CrazyCardsCalculator.Dto;
using CrazyCardsCalculator.Repository;
using System;
using System.Web.Mvc;

namespace CrazyCardsWeb.Controllers
{
    public class HomeController : Controller
    {

        private ICardsCalculator _cardsCalculator;
        private ICustomerRepository _customerRepository;
        private ICardsRepository _cardsRepository;
        public HomeController(ICardsCalculator cardsCalculator, ICustomerRepository customerRepository, ICardsRepository cardsRepository)
        {
            _customerRepository= customerRepository;
            _cardsCalculator = cardsCalculator;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(CustomerCards model)
        {
            var customerList = _customerRepository.GetCustomers("CustomerFilePath");
            var formattedStringDate = Convert.ToDateTime(String.Format("{0:dd/MM/yyyy}", model.Customer.DateOfBirth)).ToShortDateString();

            var result = _cardsCalculator.GetCustomerCards(model.Customer.FirstName, model.Customer.LastName, formattedStringDate, (int)model.Customer.EmploymentStatus, model.Customer.HouseNumber, model.Customer.PostCode);

            return View(result); 
        }
    }
}